from http import HTTPStatus
import json


def generic_response(status_code=None, data={}, message=None, **kwargs):
    return {
        "success": (status_code < 299),
        "code": status_code,
        "message": message if message else HTTPStatus(status_code).phrase,
        "data": data,
        **kwargs
    }, status_code


def clean_filters(filters):
    return {key: value for key, value in filters.items() if value is not None}


def get_where_clause_by_filters(filters):
    conditions = []
    where_clause = ""
    for column, value in filters.items():
        if isinstance(value, str):
            value = repr(value) if (
                value[0] not in ['"', "'"] and value[-1] not in ['"', "'"]
            ) else value
        conditions.append(f"{column} = {value}")
        where_clause = " AND ".join(conditions)
    return where_clause


def parse_database_information_to_dict(database_result, description):
    if not database_result:
        return []
    columns = [column[0] for column in description]
    if not is_multiple_result(database_result):
        return dict(zip(columns, database_result))
    return [dict(zip(columns, row)) for row in database_result]


def serialize_data_response(serializer, data, exclude={}):
    if type(data) is not list:
        return apply_serializer(serializer, data, exclude)
    serialized_data = list(
        map(
            lambda each_data: apply_serializer(serializer, each_data, exclude),
            data
        )
    )
    return list(
        filter(
            lambda ouput_response: ouput_response is not None,
            serialized_data
        )
    )


def apply_serializer(serializer, data, exclude):
    try:
        data_serialized = serializer(**data)
        return data_serialized.dict(exclude=exclude)
    except Exception:
        return None


def is_multiple_result(results):
    return (
        isinstance(results, tuple)
        and all(isinstance(res, tuple) for res in results)
    )


def load_rules(path="rules.json"):
    try:
        with open(path, "r") as archivo:
            return json.load(archivo)
    except Exception:
        return {}


def filter_response_with_empty_values(keys, response):
    filtered_data = []
    for each_record in response:
        quit_record = True
        for key in keys:
            if each_record.get(key):
                quit_record = False
                break
        if not quit_record:
            filtered_data.append(each_record)
    return filtered_data

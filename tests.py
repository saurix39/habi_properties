from dotenv import load_dotenv
from http import HTTPStatus
from config.custom.Api import API
from config.custom.Router import Router
from models.database.property import Property
from models.serializers.GetPropertiesQueryParams import (
    GetPropertiesQueryParams
)
from models.serializers.PropertiesResponse import PropertiesResponse
from models.serializers.SinglePropertyResponse import SinglePropertyResponse
from utils.helpers import (
    generic_response,
    clean_filters,
    get_where_clause_by_filters,
    parse_database_information_to_dict,
    serialize_data_response,
    apply_serializer,
    is_multiple_result,
    load_rules,
    filter_response_with_empty_values
)
from config.database import Connection


class TestingApp():
    def test_get_handler_arguments(self):
        def test_handler(test1, test2):
            print("argument test handler")
        arguments = {
            "test1": "test1",
            "test2": "test2",
            "test3": "test3"
        }
        expected_arguments = {
            "test1": "test1",
            "test2": "test2"
        }
        arguments_filtered = API.get_handler_arguments(
            API,
            test_handler,
            arguments,
            {},
            {}
        )

        case_1 = expected_arguments == arguments_filtered

        def test_handler(test1, test2, query_params):
            print("argument test handler")

        arguments = {
            "test1": "test1",
            "test2": "test2",
            "test3": "test3"
        }
        query_params = {
            "test_query": "test_query"
        }
        expected_arguments = {
            "test1": "test1",
            "test2": "test2",
            "query_params": {
                "test_query": "test_query"
            }
        }

        arguments_filtered = API.get_handler_arguments(
            API,
            test_handler,
            arguments,
            query_params,
            {}
        )

        case_2 = expected_arguments == arguments_filtered
        return case_1 and case_2

    def test_add_route(self):
        def test_handler():
            print("test")
        route = ("/test", "GET", test_handler, HTTPStatus.OK.value)
        test_router = Router()
        test_router.add_route(*route)
        return test_router.routes[0] == route

    def test_parse_path(self):
        test_router = Router()
        path = "/Hola"
        expected_path = "/Hola"
        parsed_path = test_router.parse_path(path)
        case_1 = expected_path == parsed_path

        path = "/Hola/{name}"
        parsed_path = test_router.parse_path(path)
        expected_path = "/Hola/(?P<name>.+)"
        case_2 = expected_path == parsed_path

        return case_1 and case_2

    def test_route_request(self):
        test_router = Router()

        @test_router.get("/hola/{name}")
        def test_handler(name):
            print("test_handler")
        path_request = "/hola/david?hola=test"
        router_result = test_router.route_request(path_request, "GET")
        expected_result = (
            test_handler,
            {"name": "david"},
            {"hola": "test"},
            HTTPStatus.NO_CONTENT.value
        )
        return expected_result == router_result

    def test_get_properties(self):
        property = Property()
        filters = {
            "city": "bogota",
            "year": 2015
        }
        expected_sql = '''
                SELECT
                    property.*,
                    status.id status_id,
                    status.name status_name
                FROM
                    property
                LEFT JOIN
                    (
                    select status_history.*
                    from status_history
                    INNER JOIN
                    (
                        SELECT
                            status_history.*,
                            MAX(update_date) AS max_date
                        FROM
                            status_history
                        GROUP BY
                            property_id
                    ) latest_history
                    ON latest_history.property_id = status_history.property_id
                    AND status_history.update_date = latest_history.max_date
                    ) latest_status ON property.id = latest_status.property_id
                LEFT JOIN status ON latest_status.status_id = status.id
                WHERE latest_status.status_id IN (3,4,5)
                AND city = 'bogota' AND year = 2015
        '''
        sql_result = property.get_properties(filters, True)
        sql_result = sql_result.strip().lower().replace(
            " ",
            ""
        ).replace("\n", "")
        expected_sql = expected_sql.strip().lower().replace(
            " ",
            ""
        ).replace("\n", "")
        return sql_result == expected_sql

    def test_get_property(self):
        property = Property()
        property_id = 8
        expected_sql = '''
                SELECT *
                FROM property
                WHERE id = 8
            '''
        sql_result = property.get_property(property_id, True)
        sql_result = sql_result.strip().lower().replace(
            " ",
            ""
        ).replace("\n", "")
        expected_sql = expected_sql.strip().lower().replace(
            " ",
            ""
        ).replace("\n", "")
        return sql_result == expected_sql

    def test_get_properties_query_params_serializer(self):
        query_params = {
            "test": "Hola",
            "year": 2015,
            "status_id": 5
        }
        expected_query_params = {
            "year": 2015,
            "status_id": 5,
            "city": None
        }
        serialized_query_params = dict(
            GetPropertiesQueryParams(**query_params)
        )
        return expected_query_params == serialized_query_params

    def test_get_properties_response_serializer(self):
        test_success = False
        response_data = {
            "id": 4,
            "address": "cll-45b Sur",
            "city": "bogota"
        }
        try:
            dict(PropertiesResponse(**response_data))
        except Exception:
            test_success = True
        return test_success

    def test_get_property_response_serializer(self):
        test_success = False
        response_data = {
            "id": 4,
            "address": "cll-45b Sur",
            "city": "bogota",
            "price": 210000,
            "description": None
        }
        try:
            dict(SinglePropertyResponse(**response_data))
            test_success = True
        except Exception:
            test_success = False
        return test_success

    def test_generic_response(self):
        expected_response = (
            {
                "success": True,
                "code": 200,
                "message": "test success",
                "data": {
                    "test": "test"
                }
            },
            200
        )
        result_response = generic_response(
            HTTPStatus.OK.value,
            {"test": "test"},
            "test success"
        )
        return result_response == expected_response

    def test_clean_filters(self):
        filters = {
            "year": 2015,
            "city": None,
            "status_id": None
        }
        expected_filters = {
            "year": 2015
        }
        result_filtering = clean_filters(filters)
        return expected_filters == result_filtering

    def test_get_where_clause(self):
        filters = {
            "year": 2015
        }
        expected_where_clause = "year = 2015"
        where_clause = get_where_clause_by_filters(filters)
        return expected_where_clause == where_clause

    def test_parse_database_result_to_dict(self):
        database_result = (4, "James", "Bond")
        database_description = (
            ('id', 2, 3),
            ("name", 2, 3),
            ("last_name", 2, 2)
        )
        expected_result = {
            "id": 4,
            "name": "James",
            "last_name": "Bond"
        }
        result_to_dict = parse_database_information_to_dict(
            database_result,
            database_description
        )
        return result_to_dict == expected_result

    def test_serialize_response(self):
        data_response = [
            {
                "id": None,
                "address": "cll 42",
                "city": "bogota",
                "status_name": "en venta",
                "price": 250000,
                "description": None
            },
            {
                "id": 45,
                "address": "cll 42",
                "city": "bogota",
                "status_name": "en venta",
                "price": 250000,
                "description": "Casa en venta",
                "status_id": 8
            }
        ]
        expected_result = [
            {
                "address": "cll 42",
                "city": "bogota",
                "status_name": "en venta",
                "price": 250000,
                "description": "Casa en venta"
            }
        ]
        result_using_serializer = serialize_data_response(
            PropertiesResponse,
            data_response,
            {"id"}
        )
        return result_using_serializer == expected_result

    def test_apply_serializer(self):
        data = {
            "id": 41,
            "address": "calle 42",
            "city": "bogota",
            "price": None,
            "description": None
        }
        expected_response = {
            "address": "calle 42",
            "city": "bogota",
            "price": None,
            "description": None
        }
        result_serialize = apply_serializer(
            SinglePropertyResponse,
            data,
            {"id"}
        )
        return expected_response == result_serialize

    def test_is_multiple_result(self):
        data_to_test = ("Louis", "Perez", 15)
        result = is_multiple_result(data_to_test)
        return not result

    def test_load_rules(self):
        expected_data = {
            "public_propery_statuses": [3, 4, 5]
        }
        data = load_rules()
        return expected_data == data

    def test_filter_empty_records(self):
        properties = [
            {
                "address": "C67 Umbras",
                "city": "belen de umbria",
                "status_name": "pre_venta",
                "price": 120000000.0,
                "description": "Casa con entrada al centro comercial"
            },
            {
                "address": "",
                "city": "",
                "status_name": "en_venta",
                "price": 0.0,
                "description": None
            },
            {
                "address": "Cra 11 A No 18 E 11",
                "city": "la virginia",
                "status_name": "pre_venta",
                "price": 90000000.0,
                "description": "Hermosa casa con 3 piezas"
            }
        ]
        expected_result = [
            {
                "address": "C67 Umbras",
                "city": "belen de umbria",
                "status_name": "pre_venta",
                "price": 120000000.0,
                "description": "Casa con entrada al centro comercial"
            },
            {
                "address": "Cra 11 A No 18 E 11",
                "city": "la virginia",
                "status_name": "pre_venta",
                "price": 90000000.0,
                "description": "Hermosa casa con 3 piezas"
            }
        ]
        filtered_properties = filter_response_with_empty_values(
            ["address", "city", "price", "description"],
            properties
        )
        return filtered_properties == expected_result

    def test_database_connection(self):
        case_1 = False
        try:
            connection = Connection()
            test = connection.test_connection()
            connection.close_connection()
            case_1 = (test and test[0] == 2)
        except Exception:
            case_1 = False
        return case_1


if __name__ == "__main__":
    load_dotenv()
    tests = []
    test = TestingApp()
    tests.append(test.test_get_handler_arguments())
    tests.append(test.test_add_route())
    tests.append(test.test_parse_path())
    tests.append(test.test_route_request())
    tests.append(test.test_get_properties())
    tests.append(test.test_get_property())
    tests.append(test.test_get_properties_query_params_serializer())
    tests.append(test.test_get_properties_response_serializer())
    tests.append(test.test_get_property_response_serializer())
    tests.append(test.test_generic_response())
    tests.append(test.test_clean_filters())
    tests.append(test.test_get_where_clause())
    tests.append(test.test_parse_database_result_to_dict())
    tests.append(test.test_serialize_response())
    tests.append(test.test_apply_serializer())
    tests.append(test.test_is_multiple_result())
    tests.append(test.test_load_rules())
    tests.append(test.test_filter_empty_records())
    tests.append(test.test_database_connection())
    flag = True
    for test in tests:
        if not test:
            flag = False
            break
    if (flag):
        print("Ran " + str(len(tests)) + " tests")
        print("OK")
    else:
        print("Failed unit testings")
        print("Failed:")
        [
            print(
                "Test " + str(idx + 1)
            ) if test is False else None for idx, test in enumerate(
                tests
            )
        ]

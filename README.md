# Habi properties
*The explanation and commands of how to run the exercises is at the end.*
<br>
<br>
In this repository there is a microservice in charge of managing the information of the habi properties, on the other hand, this repository also includes information from other microservices such as LikeManager, which is in charge of managing all the information of the interactions of the users with the properties.
Architecture suggestions are made in such a way that we can find how the microservices and their communication should be distributed, important inputs are attached such as json files where the data that must be sent by the frontend for requests to the properties microservice is located; In addition, there are the sql scripts with which we will implement the database for the likes microservice (LikeManager).
## Technologies to use

- Language: Python
- Language release: 3.11.0
- Database: MySQL
- Modules
    - http: The http module allows us to instantiate an http request server with which we are going to create our api using the rest architecture.
    - HTTPServer: With HTTPServer we can implement our http server that listens for requests through a specific port of our machine.
    - mysqlclient: mysqlclient is a library that will allow us to establish connections with the database and consult the information of the tables corresponding to the properties microservice.
    - Pydantic: Pydantic is a library that will allow us to make serilizers and validate the input/output information in a more organized and much easier way.

## How to tackle the problem?
- First exercise
    - For properties microservice
        - Devise what would be the best structure for a project that does not use a framework but that must be organized in the best way.
        - Devise what is the best strategy to access the database without using an ORM but that is safe and easy to use.
        - Devise what is the best way to distribute the routes and the functions that each of the routes will implement, which can use the same methods but with dynamic parameters, in order to have an easy to use and read API.
        - Devise which are the main functionalities of our microservice and how we can unit test them without using frameworks.
        - Devise what information I should receive from the frontend.
        - Develop the initial structure of an api without frameworks, in such a way that I can have an endpoint that returns a working "hello world" to me.
        - Develop the logic of the routes in such a way that we can establish several routes of the same http method but with different functions and parameters.
        - Develop the models that will access the database.
        - Develop the input serializers and implement them within our api routes.
        - Develop serializers for responses from our endpoints.
        - Develop the logic of each of our routes by implementing the serializers, models and structures that we have previously developed.
        - Develop the logic using a structure that allows us to do the unit tests consecutively.
        - Develop unit tests of our key functions.

    - For Like manager microservice
        - Devise what is the best strategy to extend the current model of properties, at the end of everything, we must be able to store the information of the likes that users give.
        - Seeing what are the implications in terms of performance of the different options we have to extend the current property model, we must consider what are the implications in terms of standards and best practices.
        - Diagram a structure that fits the selected idea to support the information of likes on properties meeting good standards.
        - Design the entity relationship diagram to extend the currently existing model.
        - Develop the SQL script with which we will implement/extend the relationship entity model previously designed.
- Second exercise
    - Analyze the problem.
    - Analyze the input data of the problem.
    - Analyze the output data that is expected from the algorithm.
    - Find a logical pattern in the problem.
    - Devise a way in which we can solve the problem using the logical pattern.
    - Develop the algorithm that solves the problem using the most practical functions of python

## Most relevant initial questions
- How can I make an API without using frameworks?
    - We can make use of a clean http server which allows us to receive requests from a frontend, after that we can develop all the logic that allows us to implement different routes with different parameters in the style of existing frameworks such as FastAPI or Flask, we can rely on the functionalities that Python offers us such as decorators.
- Should I extend the existing database model or should I create a new database?
    - For good practices it is better to create a new database for the new microservice, within this database you will find the necessary information for the microservice to work correctly, within this database there may be replicated information from the another database or, on the contrary, there must be a reference to the other database in such a way that one information can be related to the other, to maintain the consistency of the information we can manage a sns bus or a sqs queue so that the microservices communicate when there are changes in their information, as well as there may also be an api middleware that is in charge of relating the information of the two microservices.

## Entity relationship model 
*I must emphasize that I do not agree with using the same database for the likes microservice, for this reason my proposal is in the extra points section.*
<p align="center">
  <img src="public/assets/ER_model.jpg" alt="Entity relationship model">
</p>

### Description
In general, the relationship between registered users and properties has been added, we know this action as liking. In this case, a user can like many properties and a property can receive likes from many users, for this reason the relationship is many-to-many. There are some attributes for this relationship which are "update_date" which allows us to know which was the last record of the likes given to this property and on the other hand there is the attribute "is_active" which allows us to know if that record is active, In this way, in the event that the most recent record of the likes is not active, it means that the user removed the like from the property and if he likes it again, the action is recorded in our table again.

<br>

*The sql script extending the current model is located in the root of the project and is called like_manager.sql* 
<br>

## Extra points
### Microservice architecture
<p align="center">
  <img src="public/assets/microservices_architecture.jpg" alt="Microservice architecture">
</p>

### Description
Within this microservices architecture we can realize that we have different APIs which correspond to users, properties and the likes manager. Each of the microservices has its own database which must contain the information necessary for its operation. In this case, using an independent database will allow us to decouple the microservices. It is also important to keep the databases separate because the likes microservice is for internal use and the properties microservice is for public use, which is more convenient. that the likes manager has its own database. Maintaining a database will allow the properties microservice to make changes to its structure without affecting how the likes manager works.
Maintaining a separate database for the likes manager in the long run gives us more scalability so that the API can develop smoothly.
As we can see, we have a middleware API, this API is the most common way to extract information from different microservices and relate them depending on the information that the frontend wants to see, on the other hand there are communication alternatives for APIs such as event buses or tails.

### Entity relationship model for LikeManager microservice
<p align="center">
  <img src="public/assets/like_manager_er.jpg" alt="Entity relationship model for LikeManager microservice">
</p>

### Description
As we can see, the model only has one entity which is the history of likes, within its attributes we have references to the properties and the users, this is because it is the only information it needs for its operation, in case it is If you want to relate your information, the API middleware must be in charge of extracting and joining the information from both microservices so that it can be sent to the frontend.

## Endpoints
- /property
    - Tipe of request:
        - GET
    - Query params:
        - year: 2021
        - status_id: 3
        - city: "pereira"
- /property/{property_id}
    - Tipe of request:
        - GET
    - Path params:
        - property_id: 3

## Commands
### Use python3 in linux
- **Create virtual enviroment:** virtualenv venv
- **Install requirements:** pip install -r requirements.txt
- **Run server:** python -m config.app --port 4500
- **Run tests:** python tests.py
- **Run second exercise:** python second_exercise.py
- **Run second exercise tests:** python second_exercise_tests.py
- **flake8 command:** flake8 --exclude venv/ --output-file flake8.txt  --max-line-length 79
## Environment variables for this project
The environment variables must be stored in an .env file that must be located in the root of the project, the environment variables to use are the following:

| env name | env description |
|----------|-----------------|
| SERVER_HOST | Host for which we want to start the server, by default it is localhost |
| SERVER_PORT | Port through which we are going to set up our local server |
| DATABASE_HOST | Host with which we are going to make a connection to make use of the database |
| DATABASE_PORT | Port through which we are going to access the database |
| DATABASE_USER | Username that has access to the database |
| DATABASE_PASSWORD | Password of the user who has access to the database |
| DATABASE_SCHEMA | Name of the scheme to which we want to access |

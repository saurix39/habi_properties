from config.database import Connection
from utils.helpers import get_where_clause_by_filters
from utils.helpers import load_rules


class Property:
    def __init__(
        self,
        address=None,
        city=None,
        price=None,
        description=None,
        year=None
    ):
        self.connection = Connection()
        self.address = address
        self.city = city
        self.price = price
        self.description = description
        self.year = year
        self.query = None

    def get_properties(self, filters, is_testing=False):
        rules = load_rules()
        statuses_to_include_id = rules.get("public_propery_statuses")
        where_clause = get_where_clause_by_filters(filters)
        exists_where_clause = False
        self.query = '''
                SELECT
                    property.*,
                    status.id status_id,
                    status.name status_name
                FROM
                    property
                LEFT JOIN
                    (
                    select status_history.*
                    from status_history
                    INNER JOIN
                    (
                        SELECT
                            status_history.*,
                            MAX(update_date) AS max_date
                        FROM
                            status_history
                        GROUP BY
                            property_id
                    ) latest_history
                    ON latest_history.property_id = status_history.property_id
                    AND status_history.update_date = latest_history.max_date
                    ) latest_status ON property.id = latest_status.property_id
                LEFT JOIN status ON latest_status.status_id = status.id
            '''
        if statuses_to_include_id:
            exists_where_clause = True
            self.query += '''WHERE latest_status.status_id IN ({})'''.format(
                ",".join(str(status) for status in statuses_to_include_id)
            )

        if where_clause:
            self.query += '''WHERE {}'''.format(where_clause)\
                if not exists_where_clause else ''' AND {}'''.format(
                    where_clause
                )
        if is_testing:
            return self.query
        self.connection.connect()
        self.connection.execute(self.query)
        database_result, result_description = self.connection.get_information()
        self.connection.close_connection()
        return database_result, result_description

    def get_property(self, property_id, is_testing=False):
        self.query = '''
                SELECT *
                FROM property
                WHERE id = {}
            '''.format(property_id)
        if is_testing:
            return self.query
        self.connection.connect()
        self.connection.execute(self.query)
        (
            database_result,
            result_description
        ) = self.connection.get_one_of_information()
        self.connection.close_connection()
        return database_result, result_description

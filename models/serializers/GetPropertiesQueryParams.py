from pydantic import BaseModel, Field
from typing import Optional


class GetPropertiesQueryParams(BaseModel):
    year: Optional[int] = Field(
        description="Year of construction",
        default=None,
        pre=True
    )
    city: Optional[str] = Field(
        description="City where the property is located",
        default=None,
        pre=True
    )
    status_id: Optional[int] = Field(
        description="State in which the property is located",
        default=None,
        pre=True
    )

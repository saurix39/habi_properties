from pydantic import BaseModel
from typing import Union


class SinglePropertyResponse(BaseModel):
    id: int
    address: Union[str, None]
    city: Union[str, None]
    price: Union[float, None]
    description: Union[str, None]

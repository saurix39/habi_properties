from pydantic import BaseModel
from typing import Union


class PropertiesResponse(BaseModel):
    id: int
    address: str
    city: str
    status_name: str
    price: float
    description: Union[str, None]

use habi_db;

create table like_history(
    id int(11) not null auto_increment,
    is_active boolean not null default true,
    update_date datetime not null default current_timestamp(),
    user_id int(11) not null,
    property_id int(11) not null,
    primary key(id),
    foreign key(user_id) references auth_user(id),
    foreign key(property_id) references property(id)
);

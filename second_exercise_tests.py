from second_exercise import (
    change_elements_type,
    process_entry_data
)


class TestingAlgorithm():
    def test_change_elements_type(self):
        myArray = ["12", "", "54"]
        expected_array = [[1, 2], [], [5, 4]]
        result_array = change_elements_type(myArray, int)
        case_1 = expected_array == result_array
        myArray = [[1, 2], [], [5, 4]]
        expected_array = [["1", "2"], [], ["5", "4"]]
        result_array = change_elements_type(myArray, str)
        case_2 = expected_array == result_array
        return case_1 and case_2

    def test_process_entry_data(self):
        myArray = (1, 3, 2, 0, 7, 8, 1, 3, 0, 6, 7, 1)
        expected_string = " 123 1378 167 "
        result_string = process_entry_data(myArray)
        case_1 = expected_string == result_string
        myArray = (2, 1, 0, 0, 3, 4)
        expected_string = " 12 X 34 "
        result_string = process_entry_data(myArray)
        case_2 = expected_string == result_string
        return case_1 and case_2


if __name__ == "__main__":
    tests = []
    test = TestingAlgorithm()
    tests.append(test.test_change_elements_type())
    tests.append(test.test_process_entry_data())
    flag = True
    for test in tests:
        if not test:
            flag = False
            break
    if (flag):
        print("Ran " + str(len(tests)) + " tests")
        print("OK")
    else:
        print("Failed unit testings")
        print("Failed:")
        [
            print(
                "Test " + str(idx + 1)
            ) if test is False else None for idx, test in enumerate(
                tests
            )
        ]

from config.custom.Router import Router
from http import HTTPStatus
from models.serializers.GetPropertiesQueryParams import (
    GetPropertiesQueryParams
)
from models.serializers.PropertiesResponse import (
    PropertiesResponse
)
from models.serializers.SinglePropertyResponse import (
    SinglePropertyResponse
)
from utils.helpers import (
    generic_response,
    clean_filters,
    parse_database_information_to_dict,
    serialize_data_response,
    filter_response_with_empty_values
)
from models.database.property import Property


properties_router = Router()


@properties_router.get(
    "/property/{property_id}",
    status_code=HTTPStatus.OK.value
)
def get_property(property_id):
    try:
        (
            property,
            property_result_description
        ) = Property().get_property(property_id)

        property = parse_database_information_to_dict(
            property,
            property_result_description
        )

        property_data_serialized = serialize_data_response(
            SinglePropertyResponse,
            property,
            {"id"}
        )

        return generic_response(
            HTTPStatus.OK.value,
            property_data_serialized,
            "Property obtained succesfully"
        )
    except Exception as e:
        print(e)
        return generic_response(
            status_code=HTTPStatus.UNPROCESSABLE_ENTITY.value,
            message="Invalid query params"
        )


@properties_router.get("/property", status_code=HTTPStatus.OK.value)
def get_properties(query_params):
    try:
        validated_query_params = GetPropertiesQueryParams(**query_params)
        clean_query_params = clean_filters(dict(validated_query_params))

        (
            properties,
            properties_result_description
        ) = Property().get_properties(clean_query_params)

        properties = parse_database_information_to_dict(
            properties,
            properties_result_description
        )

        properties_serialized = serialize_data_response(
            PropertiesResponse,
            properties,
            {"id"}
        )

        filtered_properties = filter_response_with_empty_values(
            ["address", "city", "price", "description"],
            properties_serialized
        )

        return generic_response(
            HTTPStatus.OK.value,
            filtered_properties,
            "Properties obtained succesfully"
        )
    except Exception as e:
        print(e)
        return generic_response(
            status_code=HTTPStatus.UNPROCESSABLE_ENTITY.value,
            message="Invalid query params"
        )

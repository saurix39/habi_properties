from config.custom.Router import Router
from http import HTTPStatus


health_router = Router()


@health_router.get("/health")
def health():
    return {
        "status": "ok"
    }, HTTPStatus.OK.value

# Algorithm
def change_elements_type(array, type_to_change):
    return [
        [
            type_to_change(value)
            for value in segment
        ] for segment in array
    ]


def process_entry_data(myArray):
    myArray = "".join(str(value) for value in myArray)
    myArray = myArray.split("0")
    myArray = change_elements_type(myArray, int)
    [segment.sort() for segment in myArray]
    myArray = change_elements_type(myArray, str)
    text_to_show = ""
    for idx, segment in enumerate(myArray):
        text_to_show += " " if idx == 0 else ""
        if segment:
            text_to_show += "".join(segment) + " "
        else:
            text_to_show += "X" + " "
    return text_to_show


if __name__ == "__main__":
    # Predefined code
    myArray = (1, 3, 2, 0, 7, 8, 1, 3, 0, 6, 7, 1)
    print(process_entry_data(myArray))

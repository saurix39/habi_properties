from http.server import HTTPServer
from dotenv import load_dotenv
import argparse
import os
from config.custom.Api import API


__import__("config.urls")
load_dotenv()


host = os.getenv("SERVER_HOST") if os.getenv("SERVER_HOST") else 'localhost'
port = int(os.getenv("SERVER_PORT")) if os.getenv("SERVER_PORT") else 5000

parser = argparse.ArgumentParser(
    description="Run the HTTP server with host and port options."
)

parser.add_argument(
    "--host",
    default=host,
    help="Host address to bind the server."
)

parser.add_argument(
    "--port",
    type=int,
    default=port,
    help="Port number to bind the server."
)

args = parser.parse_args()

server_address = (args.host, args.port)
httpd = HTTPServer(server_address, API)

print(f"Server running at http://{args.host}:{args.port}")
httpd.serve_forever()

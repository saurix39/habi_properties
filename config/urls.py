from config.custom.Api import API
from routers.health import health_router
from routers.properties import properties_router

API.add_router(health_router)
API.add_router(properties_router)

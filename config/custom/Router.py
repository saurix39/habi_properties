import re
from http import HTTPStatus
from urllib.parse import parse_qsl, urlparse


class Router:
    def __init__(self) -> None:
        self.routes = []

    def add_route(self, path, method, handler, status_code):
        self.routes.append(
            (
                self.parse_path(path),
                method,
                handler,
                status_code
            )
        )

    def parse_path(self, path):
        segments = path.split('/')
        parsed_segments = []
        for segment in segments:
            if segment.startswith('{') and segment.endswith('}'):
                parsed_segments.append(f"(?P<{segment[1:-1]}>.+)")
            else:
                parsed_segments.append(segment)
        return f"{'/'.join(parsed_segments)}"

    def route_request(self, path, method):
        for route, route_method, handler, status_code in self.routes:
            path_parts = path.split('?', 1)
            path_without_query_params = path_parts[0]

            match = re.match(route, path_without_query_params)

            if match and route_method == method:
                path_params = match.groupdict()
                parsed_url = urlparse(path)
                query_params = dict(parse_qsl(parsed_url.query))
                return handler, path_params, query_params, status_code
        return None, None, None, None

    def get(self, path, status_code=HTTPStatus.NO_CONTENT.value):
        def decorator(handler):
            self.add_route(path, "GET", handler, status_code)
            return handler
        return decorator

    def post(self, path, status_code=HTTPStatus.NO_CONTENT.value):
        def decorator(handler):
            self.add_route(path, "POST", handler, status_code)
            return handler
        return decorator

    def put(self, path, status_code=HTTPStatus.NO_CONTENT.value):
        def decorator(handler):
            self.add_route(path, "PUT", handler, status_code)
            return handler
        return decorator

    def patch(self, path, status_code=HTTPStatus.NO_CONTENT.value):
        def decorator(handler):
            self.add_route(path, "PATCH", handler, status_code)
            return handler
        return decorator

    def delete(self, path, status_code=HTTPStatus.NO_CONTENT.value):
        def decorator(handler):
            self.add_route(path, "DELETE", handler, status_code)
            return handler
        return decorator

from http.server import BaseHTTPRequestHandler
from http import HTTPStatus
import inspect
import json


class API(BaseHTTPRequestHandler):
    routers = []

    def do_GET(self):
        self.execute_handler_by_path("GET")

    def do_POST(self):
        self.execute_handler_by_path("POST")

    def do_PUT(self):
        self.execute_handler_by_path("PUT")

    def do_PATCH(self):
        self.execute_handler_by_path("PATCH")

    def do_DELETE(self):
        self.execute_handler_by_path("DELETE")

    def execute_handler_by_path(self, method):
        request_body = self.get_request_body()
        for router in API.routers:
            (
                handler,
                args,
                query_params,
                status_code
            ) = router.route_request(self.path, method)
        self.handle_request(
            handler,
            args,
            query_params,
            request_body,
            status_code
        )

    def handle_request(
        self,
        handler,
        args,
        query_params,
        request_body,
        status_code
    ):
        if handler:
            args = self.get_handler_arguments(
                handler,
                args,
                query_params,
                request_body
            )
            response, handler_status_code = handler(**args)
            self.send_response(
                handler_status_code
                if handler_status_code
                else status_code
            )
            self.send_header("Content-type", "application/json")
            self.end_headers()
            self.wfile.write(json.dumps(response).encode('utf-8'))
        else:
            self.send_response(HTTPStatus.NOT_FOUND.value)
            self.send_header("Content-type", "application/json")
            self.end_headers()
            self.wfile.write(json.dumps({
                "code": HTTPStatus.NOT_FOUND.value,
                "message": HTTPStatus.NOT_FOUND.phrase
            }).encode('utf-8'))

    def get_handler_arguments(
        self,
        handler,
        arguments,
        query_params,
        request_body
    ):
        handler_signature = inspect.signature(handler)
        handler_args = handler_signature.parameters.keys()

        arguments["query_params"] = query_params
        arguments["request_body"] = request_body

        return {
            arg: value
            for arg, value
            in arguments.items()
            if arg in handler_args
        }

    def get_request_body(self):
        if not self.headers['Content-Length']:
            return None
        content_length = int(self.headers['Content-Length'])
        request_body = self.rfile.read(content_length)
        decoded_body = request_body.decode('utf-8')
        if not decoded_body:
            return None
        return json.loads(decoded_body)

    @classmethod
    def add_router(cls, router):
        cls.routers.append(router)

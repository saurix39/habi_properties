import os
import MySQLdb


class Connection:
    def __init__(self) -> None:
        self.db_config = {
            "host": os.getenv("DATABASE_HOST"),
            "user": os.getenv("DATABASE_USER"),
            "password": os.getenv("DATABASE_PASSWORD"),
            "db": os.getenv("DATABASE_SCHEMA"),
            "port": int(
                os.getenv("DATABASE_PORT")
            ) if os.getenv("DATABASE_PORT") else None
        }
        self.connection = None
        self.cursor = None
        self.database_response = None

    def connect(self):
        self.connection = MySQLdb.connect(**self.db_config)
        self.cursor = self.connection.cursor()

    def execute(self, query):
        self.cursor.execute(query)

    def get_one_of_information(self):
        return self.cursor.fetchone(), self.cursor.description

    def get_information(self):
        return self.cursor.fetchall(), self.cursor.description

    def close_connection(self):
        self.cursor.close()
        self.connection.close()

    def test_connection(self):
        self.connect()
        self.execute("SELECT 1+1")
        result, result_description = self.get_one_of_information()
        return result
